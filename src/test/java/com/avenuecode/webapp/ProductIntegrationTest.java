package com.avenuecode.webapp;

import java.math.BigDecimal;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.avenuecode.webapp.service.dao.ProductDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml","classpath:testDataSourceContext.xml"})
@Transactional
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class, DirtiesContextTestExecutionListener.class})
public class ProductIntegrationTest extends TestCase{
	
	@Autowired
	private ProductDao dao;
	
	@SuppressWarnings("static-access")
	@Test
	public void shouldCreateProduct() throws Exception {
		
		Product p = new Product();
		p.setName("test product");
		p.setUnitPrice(new BigDecimal("100.00"));
		
		long id = dao.create(p);
		assertEquals(1, id);
		
		Product product = dao.read((long) 1);
		assertTrue(p.equals(product));
		
		
	}

}
