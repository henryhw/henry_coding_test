package com.avenuecode.webapp.service;

import java.math.BigDecimal;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.avenuecode.webapp.Product;
import com.avenuecode.webapp.service.dao.ProductDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml","classpath:testDataSourceContext.xml"})
@Transactional
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class, DirtiesContextTestExecutionListener.class})

public class ProductContollerTest extends TestCase {

	@Autowired
	private ProductController productController;

	@SuppressWarnings("static-access")
	@Test
	public void createProductTest() {

		CreateProductRequest request = new CreateProductRequest();
		request.setName("product1");
		Product product = productController.createProduct(request);
		assertEquals("product1", product.getName());

	}

	@Test
	public void readProductTest() {

		CreateProductRequest request = new CreateProductRequest();
		request.setName("product");		
		productController.createProduct(request);		

		Product product = productController.readProduct((long) 1);
		assertEquals("product", product.getName());

	}
	
	
	

}
