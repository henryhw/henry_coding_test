package com.avenuecode.webapp.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.avenuecode.webapp.Product;
import com.avenuecode.webapp.service.CreateProductRequest;
import com.avenuecode.webapp.service.ProductController;

@Component
@Path("/products")
public class ProductResource {
	
	@Autowired
	private ProductController controller;

	public ProductController getController() {
		return controller;
	}

	public void setController(ProductController controller) {
		this.controller = controller;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response getProductById(@PathParam("id") Long productId) {
		
		Product product = controller.readProduct(productId);
		
		ResponseBuilder builder = null;
		if (product != null) {
			builder = Response.ok(product);
		} else {
			builder = Response.status(Status.GONE);
		}
		
		return builder.type(MediaType.APPLICATION_JSON_TYPE).build();
		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createProduct(CreateProductRequest request) {
	
		Product product = controller.createProduct(request);
		
		ResponseBuilder builder = null;
		if (product != null) {
			builder = Response.status(Status.CREATED).entity(product);
		} else {
			builder = Response.status(Status.CONFLICT);
		}
		
		return builder.type(MediaType.APPLICATION_JSON_TYPE).build();
		
	}
	

}
