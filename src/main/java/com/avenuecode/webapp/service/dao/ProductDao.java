package com.avenuecode.webapp.service.dao;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.avenuecode.webapp.Product;

@Component
public class ProductDao {
	
	@Autowired
	private Session session;

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
	
	public Long create(Product product) {
		return (Long) session.save(product);
	}
	
	public Product read(Long productId) {
		return (Product) session.load(Product.class, productId);
	}
	
	public void update(Product product) {
		session.update(product);
	}

}
