package com.avenuecode.webapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.avenuecode.webapp.Product;
import com.avenuecode.webapp.service.dao.ProductDao;

@Component
public class ProductController {
	
	@Autowired
	private ProductDao dao;

	public ProductDao getDao() {
		return dao;
	}

	public void setDao(ProductDao dao) {
		this.dao = dao;
	}
	
	public Product createProduct(CreateProductRequest request) {
		
		Product product = new Product();
		product.setName(request.getName());
		product.setUnitPrice(request.getUnitPrice());
		
		dao.create(product);
		
		return product;
		
	}
	
	public Product readProduct(Long productId) {
		
		return dao.read(productId);
		
	}

}
