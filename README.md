# Java WebApp Coding Test Instructions

Your task is to develop a simple RESTful webservice that implements a common e-commerce scenario, placing and modifying orders. We have provided you with an mavenized starter template that provides you with the necessary dependencies to build, test and run a webservice using the following:

- Core tools:
    - JDK 7
    - Maven 3
- Compile scope:
    - Guava
    - slf4j/logback
    - Spring
    - Hibernate
    - Jersey / JAX-RS
    - Jackson
    - H2 embedded database
- Test scope:
    - JUnit/Hamcrest
    - Mockito
    - PowerMock
    - Spring Test
    - dbUnit

You may add additional dependencies, tools and maven plugins as you deem necessary to complete your application. 

# REST API

Your service should consist of two RESTful resources:

## Products
	
which should implement two methods:
		
- list product catalog
- read a single product by its id
			
## Orders
	
which should implement four methods:
		
- place an order
- modify and existing order
- list placed orders
- read an existing order by its id
			
Your service should use JSON as its representation format for objects

# Domain Model			
			
Your application should consist of the following entities with the following properties and methods:

## Product
	
### properties:
name       |    description
:----------|:-----------------------------------------------------
id         | a numeric identifier which is unique for each product
name       | text description of the product
unit price | the price of a single unit of this product
		
## Order
	
### properties:

name  |    description
:-----|:-----------------------------------------------------
id    | a numeric identifier which is unique for each product
items | a list of items that comprise the order
			
### methods:
name            |    description
:---------------|:---------------------------------------------------------------
number of items | returns the number of items in the order
total price     | returns the sum of the extended price of all items in the order
			
## OrderItem
	
### properties:
name     |    description
:--------|:----------------------------------------------------------
id       | a numeric identifier which is unique for each item
product  | the product which is being ordered by this item
quantity | an integer quantity of products being ordered by this item
			
### methods:
name           |    description
:--------------|:-------------------------------------------------------
extended price | the price for the item's product at the item's quantity
			
You will need to create an appropriate relational schema to persist your entities, along with any required mappings.

# Submitting your code

Once you have completed your application, you should create a private repository on bitbucket.org and share it with the user "ac-brazil-recruitment" to be evaluated. Please include any additional notes that would be helpful to evaluate, run and test your submission.

Thank you,
The AvenueCode Recruiting Team
